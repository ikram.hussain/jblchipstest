import React, { Component, Fragment } from "react";

import { debounce } from "throttle-debounce";
class Autocomplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: "",
      selectedMovie: [],
      moviesArray: []
    };
  }

  onChange(e) {
    if(this.state.selectedMovie.length<5){
      e.persist();
    const userInput = e.currentTarget.value;
    console.log(userInput);
    const filteredSuggestions = this.state.moviesArray.filter(
      suggestion =>
        suggestion.label.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );
 
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });

    debounce(500, () => {
      this.getMovies(e.target.value);
    })();
  }
  }

  deleteChips = id => {
    this.setState({
      selectedMovie: this.state.selectedMovie.filter(chip => chip.id !== id)
    });
  };
  async getMovies(title) {
    let url = "http://www.omdbapi.com/?s=" + title + "&apikey=6f0b9057";
    let response = await fetch(url);
    let movies = await response.json();
    console.log(movies);
    if (movies.Response === "True") {
      const transformedArray = movies.Search.map(
        ({ Title, imdbID, Year, Poster }) => ({
          label: Title,
          poster: Poster,
          id: imdbID,
          secondaryText: "Year- " + Year
        })
      );
      console.log(transformedArray);
      const filteredSuggestions = transformedArray.filter(
        suggestion =>
          suggestion.label
            .toLowerCase()
            .indexOf(this.state.userInput.toLowerCase()) > -1
      );
      this.setState({ moviesArray: transformedArray, filteredSuggestions });
    }
  }
  onClick = e => {
    this.state.selectedMovie.push(e);
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: "",
      selectedMovie: this.state.selectedMovie
    });
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      if (activeSuggestion !== 0) {
      console.log(activeSuggestion)
      this.state.selectedMovie.push(filteredSuggestions[activeSuggestion]);
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion],
        selectedMovie: this.state.selectedMovie
      });
    }
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  render() {
    console.log(this.state.selectedMovie);
    const {
      state: { filteredSuggestions, showSuggestions, userInput, selectedMovie }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions
              .filter(({ id }) => !selectedMovie.find(chip => chip.id === id))
              .map((suggestion, index) => {
                return (
                  <li
                    key={suggestion.id}
                    onClick={e => this.onClick(suggestion)}
                  >
                    <div> {suggestion.label}</div>
                    <div style={{ fontSize: "11px" }}>
                      {suggestion.secondaryText}
                    </div>
                  </li>
                );
              })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions</em>
          </div>
        );
      }
    }

    return (
      <Fragment>
        <div className="setmargin">

       
        <p>You Can Select Only 5 Movies</p>
    <p>Data Searching based on minimum 3 charecters</p>
    </div>
        <div className="border">
          
          {this.state.selectedMovie.map(({ id, label, poster }) => (
            <div className="chip" key={id}>
              <span className="circel">{label.charAt(0).toUpperCase()}</span>

              {label}
              <span className="closebtn" onClick={e => this.deleteChips(id)}>
                &times;
              </span>
            </div>
          ))}

          <div className="new">
            <input
              className="input-auto"
              type="text"
              autoFocus
              onChange={e => this.onChange(e)}
              onKeyDown={e => this.onKeyDown(e)}
              value={userInput}
            />
            {suggestionsListComponent}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Autocomplete;
